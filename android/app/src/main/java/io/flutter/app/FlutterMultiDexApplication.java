// Generated file.
//
// If you wish to remove Flutter's multidex support, delete this entire file.
//
// Modifications to this file should be done in a copy under a different name
// as this file may be regenerated.

package io.flutter.app;

import android.content.Context;
import androidx.annotation.CallSuper;

import org.leanflutter.plugins.flutter_qiyu.FlutterQiyuPlugin;

/**
 * Extension of {@link android.app.Application}, adding multidex support.
 */
public class FlutterMultiDexApplication extends FlutterApplication {
  @Override
  @CallSuper
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
  }

  @Override
  public void onCreate() {
    super.onCreate();
    FlutterQiyuPlugin.config(this, "41af2d1905e0920bd92fff97b7d41e66");
  }
}
